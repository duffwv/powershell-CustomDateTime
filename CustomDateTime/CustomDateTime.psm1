Function Get-LastWeekday{
[CmdletBinding()]
    Param(
	    [Parameter(ValueFromPipeline=$True)]
	    [datetime[]]$Today = (Get-Date)
    )

    Process {
	    ForEach ($date in $Today) {

		    $assert = $date.addDays(-1)
		    While (!([int]$assert.DayOfWeek % 6)) { #while not a weekday
			    $assert = $assert.addDays(-1)
		    }
		    $assert

	    }# ForEach
    }# Process
}#Function Get-LastWeekday

Function Get-DateString {
    Param(
        [Parameter(
            ValueFromPipeline = $True,
            Position = 0
        )]
        [DateTime]
        $Date = (Get-Date)
    )
    Return (Get-Date -Date $Date -Format 'yyyyMMddHHmmss')
}

Function Test-DateString {
    Param(
        [Parameter(
            Mandatory = $True,
            ValueFromPipeline = $True,
            Position = 0
        )]
        [String]
        $String
    )

    $format = "yyyyMMddHHmmss"
    $regex = "^(?<year>\d{4})(?<restofdate>(?:\d{2}){0,5})$"

    $String = $String -replace '-',''

    If($String -match $regex) {
        $format = $format[0..($String.Length -1)] -join ''
        Try {
            [datetime]::ParseExact($String,$format,[CultureInfo]::InvariantCulture) | Out-Null
            Return $True
        } Catch {
            Return $False
        }
    } Else {
        Return $False
    }
}

Function Get-DateFromDateString {
    Param(
        [Parameter(
            Mandatory = $True,
            ValueFromPipeline = $True,
            Position = 0
        )]
        [String]
        $String
    )

    $format = "yyyyMMddHHmmss"
    $regex = "^(?<year>\d{4})(?<restofdate>(?:\d{2}){0,5})$"

    $String = $String -replace '-',''

    If($String -match $regex) {
        $format = $format[0..($String.Length -1)] -join ''
        Try {
            $dateobj = [datetime]::ParseExact($String,$format,[CultureInfo]::InvariantCulture)
            Return $dateobj
        } Catch {
            Write-Error "Invalid Date String"
        }
    } Else {
        Write-Error "Invalid Date String"
    }
}